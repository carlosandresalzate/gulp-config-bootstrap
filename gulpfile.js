const browserSync = require('browser-sync').create();
const del = require('del');
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const cssClean = require('gulp-clean-css');
const concat = require('gulp-concat');
const htmlClean = require('gulp-htmlclean');
const inject = require('gulp-inject');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const webserver = require('gulp-webserver');

const paths = {
	src: 'src/**/*',
	srcPug: 'src/*.pug',
	srcScss: 'src/scss/*.scss',
	srcJs: 'src/js/*.js',

	srcBootstrapScss: 'node_modules/bootstrap/scss/bootstrap.scss',
	srcBootstrapJs: 'node_modules/bootstrap/dist/js/bootstrap.min.js',
	srcJquery: 'node_modules/jquery/dist/jquery.min.js',
	srcPopperjs: 'node_modules/popper.js/dist/umd/popper.min.js',
	srcFontAwesome: 'node_modules/font-awesome/css/font-awesome.min.css',

	tmp: 'tmp',
	tmpIndex: 'tmp/index.html',
	tmpCss: 'tmp/css/',
	tmpJs: 'tmp/js/',

	dist: 'dist',
	distIndex: 'dist/index.html', 
	distCss: 'dist/css/',
	distJs: 'dist/js/'
}

// Desarrollo

gulp.task('pug-html', () => {
	return gulp.src(paths.srcPug)
		.pipe(pug({"pretty": true}))
		.pipe(gulp.dest(paths.tmp));
});

gulp.task('scss-css', () => {
	return gulp.src([
		paths.srcFontAwesome,
		paths.srcBootstrapScss, 
		paths.srcScss
		])
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.init())
		.pipe(autoprefixer({ browsers: ['last 2 version'], cascade: true }))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.tmpCss))
		.pipe(browserSync.stream());
});

gulp.task('babel-js', () => {
	return gulp.src([
		paths.srcBootstrapJs,
		paths.srcJquery,
		paths.srcPopperjs,
		paths.srcJs
	])
		.pipe(babel())
		.pipe(gulp.dest(paths.tmpJs))
		.pipe(browserSync.stream());
});

gulp.task('fonts', () => {
	return gulp.src('node_modules/font-awesome/fonts/*')
	.pipe(gulp.dest('tmp/fonts'));
});

gulp.task('inyectar', ['copy'], () => {
	let css = gulp.src('tmp/css/*.css');
	let js = gulp.src('tmp/js/*.js');
	return gulp.src(paths.tmpIndex)
		.pipe(inject(css, {relative:true}))
		.pipe(inject(js, {relative: true}))
		.pipe(gulp.dest(paths.tmp));
});

gulp.task('serve', ['inyectar'], () => {
	browserSync.init({
		server: {
			baseDir: "./tmp/"
		}
	});
});

gulp.task('watch', ['serve'], () => {
	gulp.watch(paths.src, ['inyectar']);
	gulp.watch('node_modules/bootstrap/scss/*.scss', ['inyectar']);
	gulp.watch('src/includes/*.pug', ['inyectar']);
});

gulp.task('copy', ['pug-html', 'scss-css', 'babel-js', 'fonts']);

gulp.task('default', ['watch']);

// produccion 

gulp.task('html:prod', () => {
	return gulp.src(paths.srcPug)
		.pipe(pug())
		.pipe(htmlClean())
		.pipe(gulp.dest(paths.dist));
});

gulp.task('css:prod', () => {
	return gulp.src([paths.srcScss, paths.srcBootstrapScss, paths.srcFontAwesome])
		.pipe(sass())
		.pipe(concat('style.min.css'))
		.pipe(cssClean())
		.pipe(gulp.dest(paths.distCss));
});

gulp.task('js:prod', () => {
	return gulp.src([paths.srcJs, paths.srcBootstrapJs, paths.srcJquery, paths.srcPopperjs])
		.pipe(babel())
		.pipe(concat('script.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(paths.distJs));
});

gulp.task('fonts:prod', () => {
	return gulp.src('node_modules/font-awesome/fonts/*')
	.pipe(gulp.dest('dist/fonts'));
});

gulp.task('copy:prod', ['html:prod', 'css:prod', 'js:prod', 'fonts:prod']);

gulp.task('inject:prod', ['copy:prod'], () => {
	let css = gulp.src('dist/css/*.css');
	let js = gulp.src('dist/js/*.js');
	return gulp.src(paths.distIndex)
		.pipe(inject( css, {relative: true}))
		.pipe(inject( js, {relative: true}))
		.pipe(gulp.dest(paths.dist));
});

gulp.task('prod', ['inject:prod']);

// Limpiar!
gulp.task('clean', () => {
	del([paths.tmp, paths.dist]).then(paths => {
		console.log('delete files and folders:\n', paths.join('\n'));
	});
});

